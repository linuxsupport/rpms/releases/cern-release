Name:		cern-release
Version:	1.0
Release:	1%{?dist}
Summary:	CERN repository release file
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:    %{name}-%{version}.tgz

Requires: cern-gpg-keys

%description
This package contains yum configuration for the CERN cern-release repository

%prep
%setup -q -n %{name}-%{version}

%build

%install
install -d -m 0755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -p -m 0644 CERN-rhel9.repo %{buildroot}%{_sysconfdir}/yum.repos.d/CERN-rhel9.repo

%files
%defattr(-,root,root,-)
%{_sysconfdir}/yum.repos.d

%changelog
* Fri Sep 02 2022 Georgios Argyriou <georgios.argyriou@cern.ch> 1.0-1
- Initial release
